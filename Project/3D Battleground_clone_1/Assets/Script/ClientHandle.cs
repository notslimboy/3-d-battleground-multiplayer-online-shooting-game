﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;

public class ClientHandle : MonoBehaviour
{
    public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();

        Debug.Log($"Message from server: {_msg}");
        Clients.instance.myId = _myId;
        ClientSend.WelcomeReceived();

        Clients.instance.udp.Connect(((IPEndPoint)Clients.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    public static void Login(Packet _packet)
    {
        bool success = _packet.ReadBool();
        string message = _packet.ReadString();
        
        if (success)
        {
            Clients.instance.user = _packet.ReadObject<User>();
        }

        AuthManager.instance.statusText.text = message;

        AuthManager.instance.authPanel.SetActive(false);
        LobbyManager.instance.lobbyPanel.SetActive(true);
    }

    public static void SignUp(Packet _packet)
    {
        bool success = _packet.ReadBool();
        string message = _packet.ReadString();
        if (success)
        {
           AuthManager.instance.ResetInput();
        }
        
        AuthManager.instance.statusText.text = message;
    }

    public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();

        GameManager.instance.SpawnPlayer(_id, _username, _position, _rotation);
    }

    public static void PlayerIsReady(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _isReady = _packet.ReadBool();

        GameManager.instance.players[_id].isReady = _isReady;
    }

    public static void StartGame(Packet _packet)
    {
        Debug.Log("Start Game");
    }

    public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Vector3 _position = _packet.ReadVector3();

            GameManager.instance.players[_id].transform.position = _position;
        }
    }

    public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Quaternion _rotation = _packet.ReadQuaternion();

            GameManager.instance.players[_id].transform.rotation = _rotation;
        }
    }

    public static void PlayerDisconnected(Packet _packet)
    {
        int id = _packet.ReadInt();
        Debug.Log("Player " + id + "is disconnected from server");

        Destroy(GameManager.instance.players[id].gameObject);
    }
}
