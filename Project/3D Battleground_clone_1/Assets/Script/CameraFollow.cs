﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform playerTransform;

    private Vector3 _CameraOffset;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        _CameraOffset = transform.position - playerTransform.position;
    }

    private void LateUpdate()
    {
        Vector3 newPos = playerTransform.position + _CameraOffset;

        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
