﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class LobbyManager : MonoBehaviour
{
    public static LobbyManager instance;
    public GameObject lobbyPanel;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void ToggleIsReady()
    {
        bool isReady = GameManager.instance.players[Clients.instance.myId].isReady;

        ClientSend.isReadyInput(!isReady);
    }
}


