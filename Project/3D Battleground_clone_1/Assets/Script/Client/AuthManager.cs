﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AuthManager : MonoBehaviour
{
    public static AuthManager instance;

    public GameObject authPanel;

    public TMP_InputField usenameinput, passwordInput;
    public TMP_Text statusText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        statusText.text = "";
        Clients.instance.ConnectToServer();
    }

    public void Login()
    {
        if (usenameinput.text == "")
        {
            statusText.text = "Username must not empty";
            return;
        }
        else if(passwordInput.text == "")
        {
            statusText.text = "Password must not empty";
            return;
        }
        ClientSend.LoginInput(usenameinput.text, passwordInput.text);
    }
    
    public void SignUp()
    {
        if (usenameinput.text == "")
        {
            statusText.text = "Username must not empty";
            return;
        }
        else if(passwordInput.text == "")
        {
            statusText.text = "Password must not empty";
            return;
        }
        ClientSend.SignUpInput(usenameinput.text, passwordInput.text);
    }

    public void ResetInput()
    {
        usenameinput.text = "";
        passwordInput.text = "";
    }
}
