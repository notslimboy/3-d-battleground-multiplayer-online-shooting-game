﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public int id;
    public int coin;
    public int zeroCoin = 0;
    public float health;
    public float maxHealth;
    public string username;
    public bool isReady;
    public bool isBack;

    public SkinnedMeshRenderer model;

    public void Initialize(int _id, string _username)
    {
        id = _id;
        coin = zeroCoin;
        username = _username;
        health = maxHealth;
    }

    public void SetHealth(float _health)
    {
        health = _health;
        if (id == Clients.instance.myId)
        {
            GameUIManager.instance.SetHealth(health);
        }

        if (health <= 0)
        {
            Die();
        }
    }

    public void SetCoin(int _coin)
    {
        coin = _coin;

        if (id == Clients.instance.myId)
        {
            GameUIManager.instance.SetCoin(coin);
        }
        
        //_coin = 0;
    }

    public void Die()
    {
        model.enabled = false;
    }

    public void Respawn()
    {
        model.enabled = true;
        SetCoin(zeroCoin);
        //GameUIManager.instance.SetCoin(id);
        SetHealth(maxHealth);
    }
}
