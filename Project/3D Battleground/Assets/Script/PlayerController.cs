﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class PlayerController : MonoBehaviour
{
    public int id;
    public string username;

    public Transform camTransform;

    public bool isReady;
    public Animator PlayerAnim;


    private void Start()
    {
        PlayerAnim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            ClientSend.PlayerShoot(camTransform.forward);
        }
        
    }

    private void FixedUpdate()
    {
        SendInputToServer();
    }

    private void SendInputToServer()
    {
        bool[] _inputs = new bool[]
        {
            Input.GetKey(KeyCode.W),
            Input.GetKey(KeyCode.A),
            Input.GetKey(KeyCode.S),
            Input.GetKey(KeyCode.D)
        };

        ClientSend.PlayerMovement(_inputs);
    }

    public void SetAnimation(string _animation, bool _value)
    {
        if (_animation == "isShoot")
        {
            PlayerAnim.SetTrigger(_animation);
            return;
        }
        PlayerAnim.SetBool(_animation, _value);
    }
}
