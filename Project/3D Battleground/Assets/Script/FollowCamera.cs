﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    //public Transform playerTransform;
    private GameObject target;
    //private Vector3 position;
    private Vector3 _CameraOffset;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        //position = new Vector3(10, 10, 10);
        /*
        if (!target)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
        */
        //transform.position = playerTransform.position + position;

        _CameraOffset = transform.position;
    }

    private void LateUpdate()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            transform.position = target.transform.position + _CameraOffset;
        }
        //Vector3 newPos = playerTransform.position + _CameraOffset;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
