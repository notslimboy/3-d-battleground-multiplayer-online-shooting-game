﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CoinCount : MonoBehaviour
{
    public TextMeshProUGUI coinText;

    public void SetCoin(int _coinAmount)
    {
        coinText.text = "Coin : " + _coinAmount;
    }
}
