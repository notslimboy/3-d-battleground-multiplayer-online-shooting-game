﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Net;

public class ClientHandle : MonoBehaviour
{
    public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();

        Debug.Log($"Message from server: {_msg}");
        Clients.instance.myId = _myId;
        ClientSend.WelcomeReceived();

        Clients.instance.udp.Connect(((IPEndPoint)Clients.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    public static void Login(Packet _packet)
    {
        bool success = _packet.ReadBool();
        string message = _packet.ReadString();
        
        if (success)
        {
            Clients.instance.user = _packet.ReadObject<User>();
        }

        AuthManager.instance.statusText.text = message;

        AuthManager.instance.authPanel.SetActive(false);
        LobbyManager.instance.lobbyPanel.SetActive(true);
    }

    public static void SignUp(Packet _packet)
    {
        bool success = _packet.ReadBool();
        string message = _packet.ReadString();
        if (success)
        {
           AuthManager.instance.ResetInput();
        }
        
        AuthManager.instance.statusText.text = message;
    }

    public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();

        GameManager.instance.SpawnPlayer(_id, _username, _position, _rotation);
    }

    public static void PlayerIsReady(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _isReady = _packet.ReadBool();

        GameManager.instance.players[_id].isReady = _isReady;
        //GameUIManager.instance.SetReadyPlayer(_id);
    }

    public static void TotalIsReadyPlayer(Packet _packet)
    {
        int _totalPlayer = _packet.ReadInt();
        int _totalIsReadyPlayer = _packet.ReadInt();
        GameUIManager.instance.SetTotalPlayer(_totalPlayer);
        GameUIManager.instance.SetReadyPlayer(_totalIsReadyPlayer);
    }

    public static void PlayerIsBack(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _isBack = _packet.ReadBool();

        GameManager.instance.players[_id].isBack = _isBack;
    }

    public static void StartGame(Packet _packet)
    {
        Debug.Log("Start Game");
        //LobbyManager.instance.lobbyPanel.SetActive(false);
        //LobbyManager.instance.isReadyPanel.SetActive(true);
        SceneManager.LoadScene("Residental 1");
    }

    public static void BackLobby(Packet _packet)
    {
        //LobbyManager.instance.isReadyPanel.SetActive(false);
        SceneManager.LoadScene("LobbyNew");
    }

    public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Vector3 _position = _packet.ReadVector3();

            GameManager.instance.players[_id].transform.position = _position;
        }
    }

    public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Quaternion _rotation = _packet.ReadQuaternion();

            GameManager.instance.players[_id].transform.rotation = _rotation;
        }
    }

    public static void PlayerHealth(Packet _packet)
    {
        int _id = _packet.ReadInt();
        float _health = _packet.ReadFloat();
        
        GameManager.instance.players[_id].SetHealth(_health);
        if (_id == Clients.instance.myId)
        { 
            GameManager.instance.players[Clients.instance.myId].health = _health;
            GameUIManager.instance.SetHealth(_health);
        }
        
    }

    public static void PlayerRespawned(Packet _packet)
    {
        int _id = _packet.ReadInt();

        GameManager.instance.players[_id].Respawn();
    }

    public static void SpawnCoin(Packet _packet)
    {
        int _coinId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.SpawnCoin(_coinId, _position);
    }

    public static void DestroyCoin(Packet _packet)
    {
        int _coinId = _packet.ReadInt();

        GameManager.instance.DestroyCoin(_coinId);
    }

    public static void PlayerCoin(Packet _packet)
    {
        int _coinAmount = _packet.ReadInt();

        GameManager.instance.players[Clients.instance.myId].coin = _coinAmount;
        GameUIManager.instance.SetCoin(_coinAmount);
    }
    

    public static void SpawnProjectile(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        int _thrownByPlayer = _packet.ReadInt();

        GameManager.instance.SpawnProjectile(_projectileId, _position);
    }

    public static void ProjectilePosition(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.projectiles[_projectileId].transform.position = _position;
    }

    public static void ProjectileExploded(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.projectiles[_projectileId].Explode(_position);
    }

    public static void PlayerAnimation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _animation = _packet.ReadString();
        bool _value = _packet.ReadBool();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            GameManager.instance.players[_id].GetComponent<PlayerController>().SetAnimation(_animation,_value);
        }
    }

    public static void PlayerDisconnected(Packet _packet)
    {
        int id = _packet.ReadInt();
        Debug.Log("Player " + id + "is disconnected from server");

        Destroy(GameManager.instance.players[id].gameObject);
    }

    public static void WinnerPlayer(Packet _packet)
    {
        int winnerID = _packet.ReadInt();

        GameUIManager.instance.ShowWinner(winnerID);
    }
    
}
