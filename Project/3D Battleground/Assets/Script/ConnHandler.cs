using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using UnityEngine;
using TMPro;

public class ConnHandler : MonoBehaviour
{
    public static TcpClient Client;
    public static NetworkStream networkStream;
    public static StreamWriter writer;
    public static StreamReader reader;
    public ScoreScript score;
    public HealthScript healthScript;
    Coin coin;
    User user;
    Health health;

    private string playerUsername;
    private string messageToSend;
    private string messageDamage;
    
    [SerializeField] private bool canLogin = false;
    [SerializeField] private TMP_InputField username;
    [SerializeField] private TMP_InputField password;
    [SerializeField] private TMP_Text mesagge;
    [SerializeField] private TMP_Text inGameUserNameText; 
    [SerializeField] private GameObject panelLogin;
    [SerializeField] private GameObject panelPlayerStats;
    [SerializeField] private TMP_Text actionChat;
    [SerializeField] private int coins;
    [SerializeField] private int healths;

    private void Awake()
    {
        score = FindObjectOfType<ScoreScript>();
        healthScript = FindObjectOfType<HealthScript>();
        Client = new TcpClient("127.0.0.1", 7412);
        canLogin = false;
    }

    IEnumerator OnDoneLogin()
    {
        yield return new WaitUntil((() => canLogin));
        inGameUserNameText.text = playerUsername;
    }

    private void Start()
    {
        try
        {
            writer = new StreamWriter(Client.GetStream());
            reader = new StreamReader(Client.GetStream());
            //coins = 0;
            healths = 100;
            health = new Health(100);
            //coin = new Coin(0);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        Thread t = new Thread(ReceiveMessage);
        t.Start();
        StartCoroutine(OnDoneLogin());
    }

    void ReceiveMessage()
    {
        while (!canLogin)
        {
            messageToSend = reader.ReadLine(); // feedback from server
            messageToSend.Replace("Another User Login : ",String.Empty);
            if (messageToSend.Contains(playerUsername))
            {
                Debug.Log(messageToSend);
                canLogin = true;
            }
        }

        while (canLogin)
        {
            messageToSend = reader.ReadLine();
            Debug.Log(messageToSend);

            if (messageToSend.Contains("damage"))
            {
                Debug.Log(messageToSend);
                ReceivedHealthChange();
            }
            else if (messageToSend.Contains("coins"))
            {
                Debug.Log(messageToSend);
                //ReceiveCoinChange();
            }
            else if(messageToSend.Contains("actionChat"))
            {
                Debug.Log(messageToSend);
                ThreadManager.ExecuteOnMainThread(delegate { actionChat.text += messageToSend + "\n"; });
            }
        }
    }
    
    public void ReceivedHealthChange()
    {
        networkStream = Client.GetStream();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        health = (Health) formatter.Deserialize(networkStream);
        healthScript.healthAmount = health.value;
        healths = health.value;
        Debug.Log("Client Health`s get Changed :" + healths);
    }
    /*
    private void ReceiveCoinChange()
    {
        networkStream = Client.GetStream();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        coin = (Coin) formatter.Deserialize(networkStream);
        score.coinsAmount = coin.value;
        coins = coin.value;
        Debug.Log("Client coins get Changed :" + coins);
    }
    */
    public static void SendString(string userString)
    {
        writer.WriteLine(userString);
        writer.Flush();
    }

    public void SendToServer(byte[] userByte)
    {
        networkStream.Write(userByte, 0, userByte.Length);
        networkStream.Flush(); 
    }

    public void UserLogin()
    {
        playerUsername = username.text; // fill the username input
        user = new User(username.text, password.text);
        networkStream = Client.GetStream();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        formatter.Serialize(networkStream, user);
        mesagge.text = "Succeed";
        panelLogin.SetActive(false);
        LeanTween.move(panelPlayerStats.GetComponent<RectTransform>(), new Vector3(0, 0, 0), 0.5f);
    }

    public void AddUserCoins()
    {        
        networkStream = Client.GetStream();
        writer.WriteLine("coins");
        writer.Flush();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        //formatter.Serialize(networkStream, (int)Command.SET_COIN);
        formatter.Serialize(networkStream, coin);
    }


    public void Shoot()
    {
        networkStream = Client.GetStream();
        writer.WriteLine("damage");
        writer.Flush();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        //formatter.Serialize(networkStream, (int)Command.SET_HEALTH);
        formatter.Serialize(networkStream,health);
    }

    void OnDisable()
    {
        networkStream.Close();
        writer.Close();
        Client.Close();
    }

    sealed class CustomizedBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type returntype = null;
            string sharedAssemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            assemblyName = Assembly.GetExecutingAssembly().FullName;
            typeName = typeName.Replace(sharedAssemblyName, assemblyName);
            returntype = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return returntype;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            base.BindToName(serializedType, out assemblyName, out typeName);
            assemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }
    }
}
