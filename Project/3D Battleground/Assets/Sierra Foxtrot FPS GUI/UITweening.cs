﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UITweening : MonoBehaviour
{
    public static Clients instance;
    [SerializeField] private float timeTween = 0.3f;
    [SerializeField] private GameObject TutorialPanel;
    [SerializeField] private GameObject CreditsPanel; 
    [SerializeField] private GameObject ExitPanel;
    



    public void OpenTutorialPanel()
    {
        LeanTween.scale(TutorialPanel, new Vector3(1f, 1f, 1f), timeTween).setEaseOutCubic();
    }
    
    public void CloseTutorialPanel()
    {
        LeanTween.scale(TutorialPanel, new Vector3(0f, 0f, 0f), timeTween).setEaseOutCubic();
    }
    
    public void OpenCreditsPanel()
    {
        LeanTween.scale(CreditsPanel, new Vector3(1f, 1f, 1f), timeTween).setEaseOutCubic();
    }
    
    public void CloseCreditsPanel()
    {
        LeanTween.scale(CreditsPanel, new Vector3(0f, 0f, 0f), timeTween).setEaseOutCubic();
    }
    
    public void OpenExitPanel()
    {
        LeanTween.scale(ExitPanel, new Vector3(1f, 1f, 1f), timeTween).setEaseOutCubic();
    }
    
    public void CloseExitPanel()
    {
        LeanTween.scale(ExitPanel, new Vector3(0f, 0f, 0f), timeTween).setEaseOutCubic();
    }

    public void DoExitGame()
    {
        Application.Quit();
        Debug.Log("Application close and Disconnect From Server");
    }

}


