# Flow and Diagaram

### Game Flow

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/1fc956d4f350986c200765c4aaabad2c/Flow.png" width ="800">

### Diagram 

* #### Server Architcture 

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/42e0a2709c47b23e2e190ee63cab044e/Server_Architecture.png" width ="800">


 * #### Client Architcture 

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/379d2b39334a0f57b905097b98418f5e/Client_Architecture.png" width ="800">


# Code Documentation


Table Of Content
* [Packet](#packet-system)
* [Game Play](#main-gameplay)
* [Player Spawn](#spawning-player)
* [Player Movement](#player-movement)
* [Coin Spawn](#coin-spawn)
* [Player Shoot](#player-shooting)
* [Player Coin](#player-coin)
* [Player Health](#player-health)
* [Player Respawn](#player-respawn)
* [Win Condition](#win-condition)


### Packet System
This enum will provide several identities that will later be used when sending or receiving packages from client and server used when the data transfer process

**Server Packet**
```csharp
public enum ServerPackets
{
    welcome = 1,
    login,
    signUp,
    SpawnPlayer,
    playerIsReady,
    totalIsReadyPlayer,
    startGame,
    backLobby,
    spawnCoin,
    destroyCoin,
    playerCoin,
    playerHealth,
    playerRespawned,
    PlayerPosition,
    PlayerRotation,
    spawnProjectile,
    projectilePosition,
    projectileExploded,
    PlayerAnimation,
    winnerPlayer,
    PlayerDisconnected
}
```

**Client Packet**
```csharp
public enum ClientPackets
{
    welcomeReceived = 1,
    loginInput,
    signUpInput,
    playerShoot,
    playerThrowProjectil,
    PlayerMovement,
    isReady,
    isBack,
}
```


##### The flow of _Authentication_ process
 
1. Player must have an account, if the player already has an account player must fill the login form to login into game

    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/3ca167f3d90d429a6500fe6233de5867/image.png" width="700">

2. Player can login into the game with existing account 

    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/8dce32c4ab9a62668f84707014472cf6/image.png" width="700">

3. If the login succed then player can spawned to lobby room

    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/02173b37277ad8e75d7ef6b21525e5f9/image.png" width="700">

    
    **Authentication Process**

    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/6e13d729d4a271ff42893ff12cc60a39/LoginAndSpawn.gif" width="700">

### Lobby System

Before entering the game, there is a Lobby System that players will enter before the game takes place, this Lobby System is used to regulate players as well as the process of moving the scene in this game. The lobby system will be the place where players determine whether they are ready to enter the battlefield or not, the game can only start when the lobby system has received data if all players are ready to play, where the lobby will immediately move players who are already in the lobby to the battlefield.

### Main Gameplay

There are several main mechanisms in this game, here's an explanation of how all these mechanisms work in this online multiplayer game :

#### Spawning Player

##### The flow of _Spawning Player_ process

1. When authentication succeed, packet data from player it will checked by the server on CheckLogin(), when verification process suceed player will allow to join the game  and the server will run function SendIntoGame() to deploy the player.

    ```csharp
    public User CheckLogin(string _username, string _password)
    {
        foreach (User _user in users)
        {
            if (_username == _user.username)
            {
                if (_password != _user.password)
                {
                    authMessageError = AuthMessageError.WrongPassword;
                    return null;
                }
                    
                authMessageError = AuthMessageError.None;
                return _user;
            }
        }

        authMessageError = AuthMessageError.UserNotFound;
        return null;
    }
    ```

2. To do the spawn player process the server will run SendIntoGame() and get the SpawnPlayer() function on NetworkManager class that have been stored a data from player such as prefabs, position and other component
    ```csharp
    public void SendIntoGame(User _user)
    {
        user = _user;
        player = NetworkManager.instance.SpawnPlayer();
        player.Initialize(id, _user.username, new Vector3(0, 0, 0))
        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                if (_client.id != id)
                {
                    ServerSend.SpawnPlayer(id, _client.player);
                }
            }
        
        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                ServerSend.SpawnPlayer(_client.id, player);
            }
        }
        int totalPlayer = LobbyManager.instance.GetTotalPlayer();
        int countIsReadyPlayer = LobbyManager.instance.GetIsReadyPlayer();
        ServerSend.TotalIsReadyPlayer(totalPlayer,countIsReadyPlayer);
    }
    ```

    ```csharp
    public Player SpawnPlayer()
    {
        Player player = Instantiate(PlayerPrefabs, respawnPosition.position, Quaternion.identityGetComponent<Player>();
        player.transform.SetParent(this.transform, false);
    
        return player;
    }
    ```

3. On the SendIntoGame() function the data contains id and username from player will be used again to do the spawn player to activate the SpawnPlayer() function on the ServerSend class
    ```csharp
    user = _user;
    player = NetworkManager.instance.SpawnPlayer();
    player.Initialize(id, _user.username, new Vector3(0, 0, 0))
    ```

4. On the server side player will be shown to an object it that is contains id, username, position and rotation will be send by the Packet class
    ```csharp
    public static void SpawnPlayer(int _toClient, Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.SpawnPlayer))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.username);
            _packet.Write(_player.transform.position);
            _packet.Write(_player.transform.rotation)
            SendTCPData(_toClient, _packet);
        }
    }
    ```

   <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/cfecf8c3371e12235cab9cb89f8da34c/PlayerSpawnInspector.png" width="700">

   _Player Object Component on Inspector_


5. On the client side, the client will recieve the packet from ClientHandle class, after get the packet and SpawnPlayer() on the GameManager will be activated and then GameManager will spawn the player for the client side
    ```csharp
    // Client Side
    public void SpawnPlayer(int _id, string _username, Vector3 _position, Quaternion _rotation)
    {
        GameObject _player;
        if (_id == Clients.instance.myId)
        {
            _player = Instantiate(localPlayerPrefab, _position, _rotation);
        }
        else
        {
            _player = Instantiate(playerPrefab, _position, _rotation);
        }
        _player.transform.SetParent(this.transform, false);
        _player.GetComponent<PlayerManager>().Initialize(_id, _username);
        players.Add(_id, _player.GetComponent<PlayerManager>());
    }
    ```

   **Spawn Player Process**

   <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/6e13d729d4a271ff42893ff12cc60a39/LoginAndSpawn.gif" width="700">

  _Spawn Player Process_

#### Player Movement

##### The flow of _Player Movement_ process

1. Client get input from player 
    ```csharp
    // Input From CLient 
    private void FixedUpdate()
    {
        SendInputToServer();
    }

    private void SendInputToServer()
    {
        bool[] _inputs = new bool[]
        {
            Input.GetKey(KeyCode.W),
            Input.GetKey(KeyCode.A),
            Input.GetKey(KeyCode.S),
            Input.GetKey(KeyCode.D)
        };
        ClientSend.PlayerMovement(_inputs); // Send Input to the Server
    }
    ```

2. Client will send the input to server by the Packet class 
    ```csharp
    public static void PlayerMovement(bool[] _inputs)
    {
        using (Packet _packet = new Packet((int)ClientPackets.PlayerMovement))
        {
            _packet.Write(_inputs.Length);
            foreach (bool _input in _inputs)
            {
                _packet.Write(_input);
            }
            _packet.Write(GameManager.instance.players[Clients.instance.myId].transform.rotation;
            SendUDPData(_packet);
        }
    }
    ```

3. The function will send packets using the UDP protocol
    ```csharp
    packet.Write(GameManager.instance.players[Clients.instance.myId].transform.rotation;
    SendUDPData(_packet);
    ```

4. The server receives the packet and identifies the contents of the packet that has been sent from the client
    ```csharp
    // Incoming Packet form Client
    public static void PlayerMovement(int _fromClient, Packet _packet)
    {
        bool[] _inputs = new bool[_packet.ReadInt()];
        for (int i = 0; i < _inputs.Length; i++)
        {
            _inputs[i] = _packet.ReadBool();
        }
        Quaternion _rotation = _packet.ReadQuaternion()
        Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
    }
    ```

5. The data sent will be used to update the position and rotation of the prefab player on the server. The server will update if the player has the latest update. Updates from the server and client will be handled by GameManager

    Server Side
    ```csharp
    private void Move(Vector2 _inputDirection)
    {
        if (canWalk)
        {
            Vector3 _moveDirection = transform.right * _inputDirection.x + transform.forwar* _inputDirection.y;
            transform.position += _moveDirection * moveSpeed;
            PlayerAnim.SetBool("isWalk",true);
            ServerSend.SendPlayerAnimation(id, "isWalk",true);
        }
        
        if (_inputDirection == Vector2.zero)
        {
                
                PlayerAnim.SetBool("isWalk",false);
                PlayerAnim.SetBool("isIdle",true);
                ServerSend.SendPlayerAnimation(id, "isWalk",false);
                ServerSend.SendPlayerAnimation(id, "isIdle",true);
        }
        
        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }
    ```

    Client Side
    ```csharp
    public static void PlayerMovement(bool[] _inputs)
    {
        using (Packet _packet = new Packet((int)ClientPackets.PlayerMovement))
        {
            _packet.Write(_inputs.Length);
            foreach (bool _input in _inputs)
            {
                _packet.Write(_input);
            }
            _packet.Write(GameManager.instance.players[Clients.instance.myId].transform.rotation;
            SendUDPData(_packet);
        }
    }
    ```

    **Player Movement Process**

    Client Side
    
    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/acd1fe99a38b1371a972baf7f3983dd3/PlayerMovement11.gif" width="700">

    Server Side
    
    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/c07f57d118670ee5baf3a0c6c525a302/PlayerMovement12.gif" width="700">


#### Coin Spawn

##### The flow of _Spawn Coin_ process

1. CoinSpawn will be manage spawn coin process, this script will run when StartGame() activated
    ```csharp
    public class CoinSpawn : MonoBehaviour
    {
        public static Dictionary<int, Coin> coins = new Dictionary<int, Coin>();
        private int idCount;
        public Vector3 spawnValues;
        
        public float spawnMostWait;
        public float spawnLeastWait;
        private float spawnWait;
        public bool isSpawning;
        [Header("Prefab")]
        public Coin coinPrefab;
        private void Start()
        {
            spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
        }
        private void Update()
        {
            if (!isSpawning) return;
            
            if (spawnWait > 0)
            {
                spawnWait -= Time.deltaTime;
            } 
            else
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x),1, Random.Range(-spawnValues.z, spawnValues.z));
                spawnPosition += this.transform.position;
                ServerSend.SpawnCoin(idCount, spawnPosition);
                Coin coin = Instantiate(coinPrefab, spawnPosition, coinPrefab.transformrotation) as Coin;
                
                coin.Initialize(idCount);
                idCount++;
                coins.Add(idCount, coin);
                spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
            }
        }
    }
    ```

2. When the CoinSpawn class active, it will run the spawn process by getting the prefabs object 
    ```csharp
    private void Update()
    {
        if (!isSpawning) return;
        
        if (spawnWait > 0)
        {
            spawnWait -= Time.deltaTime;
        } 
        else
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x),1Random.Range(-spawnValues.z, spawnValues.z));
            spawnPosition += this.transform.position;
            ServerSend.SpawnCoin(idCount, spawnPosition); // send to client
            Coin coin = Instantiate(coinPrefab, spawnPosition, coinPrefab.transformrotation) aCoin; // Spawn Coin from Server
            coin.Initialize(idCount);
            idCount++;
            coins.Add(idCount, coin);
            spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
        }
    }
    ```

3. This script will be used to determine the coins id, number of coins spawn, spawn location from each coin and time delay between spawn coin
    ```csharp
    public static Dictionary<int, Coin> coins = new Dictionary<int, Coin>();
    private int idCount;
    public Vector3 spawnValues;
    
    public float spawnMostWait;
    public float spawnLeastWait;
    private float spawnWait;
    public bool isSpawning;
    ```

4. CoinSpawn will send id and each position that have been spawn through ServerSend. In the ServerSend class there is function SpawnCoin() that will send id, position by the packet to the client

    ```csharp
    public static void SpawnCoin(int _coinId, Vector3 _position)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnCoin))
        {
            _packet.Write(_coinId);
            _packet.Write(_position);
            SendTCPDataToAllPlayers(_packet);
        }
    }
    ```

5. Client will recieve the server packet and will be handle by ClientHandle and will run by the GameManager class

    ClientHandle.cs
    ```csharp
    public static void SpawnCoin(Packet _packet)
    {
        int _coinId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        GameManager.instance.SpawnCoin(_coinId, _position);
    }
    ```

    GameManager.cs
    ```csharp
    public void SpawnCoin(int _coinId, Vector3 _position)
    {
        Coin coin = Instantiate(coinPrefab, _position, coinPrefab.transform.rotation) as Coin;
        coin.Initialize(_coinId);
        coins.Add(_coinId, coin);
    }
    ```
    
    **Spawn Coin Process**
    
    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/ac74c9080a331a889509c568d02cee3e/CoinSpawn.gif" width="700">


#### Player Shooting

##### The flow of _Player Shooting_ process

1. Shooting start from the client side. Client receive from player on Update() and it will automaticly actived PlayerShoot() on ClientSend class

    ```csharp
    private void Update()
    {
        float translation = Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime;
        float rotation = Input.GetAxis("Horizontal") * playerRotationSpeed * Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);
        
        // Input From Player
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }
    ```
    Actived by the ClientSend Class
    ```csharp
    public static void PlayerShoot(Vector3 _facing)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerShoot))
        {
            _packet.Write(_facing);
            SendTCPData(_packet);
        }
    }    

    ```

2. In PlayerShoot() on ClientSend parameter will send to the server through the Packet using TCP
    ```csharp
    public static void PlayerThrowProjectil(Vector3 _facing)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerThrowProjectil))
        {
            _packet.Write(_facing);

            SendTCPData(_packet);
        }
    }
    ```

3. On the Server side ServerHandle class will recieve packet from client and will activated by Player class

    ```csharp
    public static void PlayerShoot(int _fromClient, Packet _packet)
    {
        Vector3 _shootDirection = _packet.ReadVector3();
        Server.clients[_fromClient].player.Shoot(_shootirection);
        Server.clients[_fromClient].player.ThrowItem(_shootDirection);
    }
    ```

4. On Shoot() will activated the animation and give damage to other player where other player collide with bullet object

    ```csharp
    if (Physics.Raycast(shootOrigin.position, _viewDirection, out RaycastHit _hit, 25f))
    {
        PlayerAnim.SetTrigger("isShoot");
        ServerSend.SendPlayerAnimation(id,"isShoot",false);
        if (_hit.collider.CompareTag("Player"))
        {
            _hit.collider.transform.parent.gameObject.GetComponent<Player>().TakeDama(50f);
        }
    }    
    ```

5. As for ThrowItem(), it immediately activates the InstantiateProjectile() function in the NetworkManager script to issue a projectile

    ```csharp
    public void ThrowItem(Vector3 _viewDirection)
    {
        NetworkManager.instance.InstantiateProjectile(shootOrigin).Initializ(_viewDirection,   throwForce, id);
    }
    ```

6. The InstantiateProjectile() function which is connected to the projectile script, is used to instantiate data from the projectile starting from prefab, position, and quaternion
    ```csharp
    public Projectil InstantiateProjectile(Transform _shootOrigin)
    {
        return Instantiate(projectilePrefab, _shootOrigin.position + _shootOrigin.forward *0  7f, Quaternion.identity).GetComponent<Projectil>();
    }
    ```

7. In the projectile script there will be several active functions, starting from the Start() function which will spawn the projectile and send data to the client via ServerSend
    ```csharp
    private void Start()
    {
        id = nextProjectileId;
        nextProjectileId++;
        projectiles.Add(id, this);
        ServerSend.SpawnProjectile(this, thrownByPlayer);
        rigidBody.AddForce(initialForce);
        StartCoroutine(ExplodeAfterTime());
    }
    ```

8. Next is the FixedUpdate() function which will update the position of the projectile that has been shot and send the data via ServerSend so that the position on the Client will also be updated.

    ```csharp
    private void FixedUpdate()
    {
        ServerSend.ProjectilePosition(this);
    }
    ```

9. Then there is the Explode() function which is useful when the projectile hits an object with the "Player" tag, it will damage the player object while activating an explosion effect. All this data will also be sent via ServerSend to be updated on the Client
    ```csharp
    private void Explode()
    {
        ServerSend.ProjectileExploded(this)
        Collider[] _colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider _collider in _colliders)
        {
            if (_collider.CompareTag("Player"))
            {
                //_collider.GetComponent<Player>().TakeDamage(explosionDamage);
                _collider.transform.parent.gameObject.GetComponent<Player>().TakeDamage(25f);
            }
        
        Destroy(gameObject);
    }
    ```

10. In the ServerSend script, the three functions above will send packets via the TCP protocol
    ```csharp
    public static void SpawnProjectile(Projectil _projectile, int _thrownByPlayer)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnProjectile))
        {
            _packet.Write(_projectile.id);
            _packet.Write(_projectile.transform.position);
            _packet.Write(_thrownByPlayer)
            SendTCPDataToAll(_packet);
        }

    public static void ProjectilePosition(Projectil _projectile)
    {
        using (Packet _packet = new Packet((int)ServerPackets.projectilePosition))
        {
            _packet.Write(_projectile.id);
            _packet.Write(_projectile.transform.position)
            SendTCPDataToAll(_packet);
        }

    public static void ProjectileExploded(Projectil _projectile)
    {
        using (Packet _packet = new Packet((int)ServerPackets.projectileExploded))
        {
            _packet.Write(_projectile.id);
            _packet.Write(_projectile.transform.position)
            SendTCPDataToAll(_packet);
        }
    }
    ```

11. All packets of the three functions will be received in the ClientHandle according to their respective data recipients, where everything will be directly connected to the GameManager
    ```csharp
    public static void SpawnProjectile(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        int _thrownByPlayer = _packet.ReadInt();
        GameManager.instance.SpawnProjectile(_projectileId, _position);
    }
    public static void ProjectilePosition(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        GameManager.instance.projectiles[_projectileId].transform.position = _position;
    }
    public static void ProjectileExploded(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        GameManager.instance.projectiles[_projectileId].Explode(_position);
    }

    ```


12. The ProjectileExploded() function will be connected to the Explode() function in the ProjectileManager script, where the Explode() function will function to instantiate the Explode effect and destroy the object.
    ```csharp
    public void Explode(Vector3 _position)
    {
        transform.position = _position;
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
    ```

13. In the SpawnProjectile() function in GameManager, there will be an Instantiate on the Projectile object and the Client can finally shoot and issue the projectile
    ```csharp
    public void SpawnProjectile(int _id, Vector3 _position)
    {
        GameObject _projectile = Instantiate(projectilePrefab, _position, Quaternion.identity);
        _projectile.GetComponent<ProjectileManager>().Initialize(_id);
        projectiles.Add(_id, _projectile.GetComponent<ProjectileManager>());
    }
    ```

    **Player  Shoot Process**
    
    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/e7ea3477c8c8e2764d5f3ecd9c1fa178/Shoot.gif" width="700">

#### Player Coin

##### The flow of _Player Coins_ process

1. The player coin is a system that allows players to get coins after colliding with coin objects in Battlefield, this system starts from the onTriggerEnter() function in the coin script on the server, where if the coin object collides with an object with the "Player" tag then the function AddCoins() in a Player script will be active
    ```csharp
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            ScoreScript.inst.coinsAmount++;
            Destroy(gameObject);
        }
    }
    ```

2. This AddCoins() function will add the number of coins owned by the Player to the stats, as well as send the latest data from the number of coins via the PlayerCoin() function on ServerSend.
    ```csharp
    public static void PlayerCoin(int _toClient, int _coinAmount)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerCoin))
        {
            _packet.Write(_coinAmount);
            SendTCPData(_toClient, _packet);
        }
    }
    ```

3. The PlayerCoin() function itself has a function to send data from updating the number of coins owned by the player so that the Player object in the Client also gets the same update, where the packet sent via the PlayerCoin() function contains the player id and the latest coin number from the player.
    ```csharp
    public void AddCoins()
    {
        coin++;
        ServerSend.PlayerCoin(id, coin)
        if (coin == 10)
        {
            ServerSend.WinnerPlayer(id);
            coin = zeroCoin;
        }
    }
    ```

4. PlayerCoin() in the ClientHandle will receive the packet sent from the server and directly connect it to the GameManager to update the number of coins on the player with the appropriate id.
    ```csharp
    public static void PlayerCoin(Packet _packet)
    {
        int _coinAmount = _packet.ReadInt();
        GameManager.instance.players[Clients.instance.myId].coin = _coinAmount;
        GameUIManager.instance.SetCoin(_coinAmount);
    }
    ```

5. In addition to going to GameManager, to display the number of coins in the UI, the data that has been obtained by PlayerCoin() will also be passed to the SetCoin() function in GameUIManager
    ```csharp
    public void SetCoin(int _coinAmount)
    {
        coinText.text = "Coin : " + _coinAmount;
    }
    ```

    **Player Coin**
    
    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/a18536cba574935a849ae07497706476/Coins.gif" width="700">

#### Player Health

##### The flow of _Player Health_ process

1. Player Health is a system that is used to regulate the amount of health of the Player after being hit by a projectile, therefore this feature starts from the Explode() function in the projectile script on the server-side, wherein that function, if the projectile experiences a collision with the object tag "Player", then the TakeDamage() function will be active.
    ```csharp
    public void TakeDamage(float _damage)
    {
        if (health <= 0f)
        {
            return;
        
        health -= _damage;
        if (health <= 0f)
        {
            health = 0f;
            transform.position = NetworkManager.instance.respawnPosition.position;
            ServerSend.PlayerPosition(this);
            StartCoroutine(PlayerRespawn());
        
        ServerSend.PlayerHealth(this);
    }
    ```

2. TakeDamage() itself will set the health on the player after being hit by the projectile, the number of health will be updated and the data will be sent to the Client via the PlayerHealth() function on ServerSend
    ```csharp
    public static void PlayerHealth(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerHealth))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.health)
            SendTCPDataToAll(_packet);
        }
    }
    ```


3. PlayerHealth() will send health update data to the client by sending a packet containing the latest health id and number
    ```csharp
    using (Packet _packet = new Packet((int)ServerPackets.playerHealth))
    {
        _packet.Write(_player.id);
        _packet.Write(_player.health
        SendTCPDataToAll(_packet);
    }
    ```

4. The PlayerHealth() function on the ClientHandle will receive a packet from the server and will continue to update the health data on the player with the appropriate id via GameManager.
    ```csharp
    public static void PlayerHealth(Packet _packet)
    {
        int _id = _packet.ReadInt();
        float _health = _packet.ReadFloat();
        
        GameManager.instance.players[_id].SetHealth(_health);
        if (_id == Clients.instance.myId)
        { 
            GameManager.instance.players[Clients.instance.myId].health = _health;
            GameUIManager.instance.SetHealth(_health);
        }
        
    }
    ```

5. Update data from health will also be used by the SetHealth() function in GameUIManager to display the latest health data in the Ui Client.
    ```csharp
    public void SetHealth(float _healthAmount)
    {
        healthText.text = "Health : " + _healthAmount;
    }
    ```

    **Health Process**

    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/4f94b8686f211458d6963b931b3f70e9/Health-min.gif" width="700">

#### Die & Respawn System

##### The flow of _Player Respawn_ process

1. The Die & Respawn System starts from the TakeDamage() function in the Player script on the server-side, in the TakeDamage() function when the health of the player has reached a value less than equal to 0, then the position of the player will be returned to the spawn spot, and position updates This is sent to the client via the PlayerPosition() function on ServerSend, the health update will also be sent via the PlayerHealth() function.
    ```csharp
    if (health <= 0f)
    {
        return;

    health -= _damage;
    if (health <= 0f)
    {
        health = 0f;
        transform.position = NetworkManager.instance.respawnPosition.position;
        ServerSend.PlayerPosition(this);
        StartCoroutine(PlayerRespawn());

    ServerSend.PlayerHealth(this);
    }
    ```

2. Same as sending packets for Player health before, here sending packets via the PlayerHealth() function which contains the id and update health
    ```csharp
    public static void PlayerHealth(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerHealth))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.health)
            SendTCPDataToAll(_packet);
        }
    }
    ```

3. In addition to PlayerHealth(), the PlayerPosition() function will also send a packet to the client with the contents of id and transform.position, after sending the position, this function will also coroutine the PlayerRespawn() function.
    ```csharp
    private IEnumerator PlayerRespawn()
    {
        yield return new WaitForSeconds(5f)
        health = maxHealth;
        coin = zeroCoin;
        ServerSend.PlayerRespawned(this);
    }
    ```

4. The PlayerRespawn() function on this server respawns the player object with the initial default stats, and the data is also sent to the client via the PlayerRespawned() function on ServerSend.
    ```csharp
    public static void PlayerRespawned(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerRespawned))
        {
            _packet.Write(_player.id)
            SendTCPDataToAll(_packet);
        }
    }
    ```

5. PlayerRespawned() will send data in the form of id from player to client
    ```csharp
    using (Packet _packet = new Packet((int)ServerPackets.playerRespawned))
    {
        _packet.Write(_player.id
        SendTCPDataToAll(_packet);
    }
    ```

6. Returns the health packet, this packet is received by the PlayerHealth() function which will be directly connected to the GameManager and GameUIManager
    ```csharp
    public void SetHealth(float _healthAmount)
    {
        healthText.text = "Health : " + _healthAmount;
    }
    ```

7. In PlayerControllerManager, health that is already in the value 0, will be entered in the SetHealth function, because the heath value is equal to 0 it will activate the Die() function
    ```csharp
    public void SetHealth(float _health)
    {
        health = _health;
        if (id == Clients.instance.myId)
        {
            GameUIManager.instance.SetHealth(health);
        }

        if (health <= 0)
        {
            Die();
        }
    }
    ```

8. The Die() function has a function to remove the player object because its health is already 0.
    ```csharp
    public void Die()
    {
        model.enabled = false;
    }
    ```

9. Returning to Player Respawn, the data from the Respawn packet is received by the PlayerRespawned function in the ClientHandle, and directly passed to the GameManager connected to the Respawn() function in the PlayerManager script.
    ```csharp
    public static void PlayerRespawned(Packet _packet)
    {
        int _id = _packet.ReadInt();
        GameManager.instance.players[_id].Respawn();
    }
    ```

10. This Respawn() function is used to reactivate the previously lost player object, return its stats to default, and because previously the player's position has been reset to the initial position with the PlayerPosition() function which has received new data from the Server, then the respawn position is also in the initial position
    ```csharp
    public void Respawn()
    {
        model.enabled = true;
        SetCoin(zeroCoin);
        SetHealth(maxHealth);
    }
    ```
    **Respawn System**
    
    <img src="Document/Spawn.gif" width="700">

#### Win Condition

##### The flow of _Win - Lose System_ process

1. Win-Lose system starts from the AddCoins() function on the Server, where in this function if the player's coin is equal to a certain amount (here 10) it will activate the WinnerPlayer() function on ServerSend
    ```csharp
    public void AddCoins()
    {
        coin++;
        ServerSend.PlayerCoin(id, coin)
        if (coin == 10)
        {
            ServerSend.WinnerPlayer(id);
            coin = zeroCoin;
        }
    }
    ```

2. The WinnerPlayer() function will send client data in the form of the id of the player who has received 10 coins via the TCP protocol to all clients
    ```csharp
    public static void WinnerPlayer(int _clientId)
    {
        using (Packet _packet = new Packet((int)ServerPackets.winnerPlayer))
        {
            _packet.Write(_clientId);
            SendTCPDataToAll(_packet);
        }
    }
    ```

3. Data from this server will be received in packet form by WinnerPlayer() function in ClientHandle and directly connected to ShowWinner() function in GameUIManager
    ```csharp
    public void ShowWinner(int _winnerId)
    {
        if (Clients.instance.myId == _winnerId)
        {
            winCanvas.SetActive(true);
        }
        else
        {
            loseCanvas.SetActive(true);
        }
    }
    ```

    **Winner - Lose System**

    <img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/8b2210bf12fabc8597d1ad90fca3b0bb/image.png" width="700">