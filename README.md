# 3D Battleground - Multiplayer Online Shooting Game

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/a395de989a01dfecc7c3f2705a13d12c/Screenshot_37.png" width ="800">

3D Battleground is an online multiplayer game with Third Person Shooter gameplay that involves all players to be able to collect as many coins as possible in order to win the game. Players will be faced with several obstacles in the form of other players. All players will fight each other by shooting at opponents who are trying to get coins. Players who are shot will be respawned and reset the progress of the coins they already have back to 0


#### Platform 

<img src="https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white" width="100">

#### Genre
Casual, Shooter, Battleground

#### Technologies
* #### Hosting (Digital Ocean VPS (Singapore Data Center)) 
    <img src="https://miro.medium.com/max/1400/1*y4my-C7YQuZ0T-6E2OW2Ow.png" width="70">

* #### Unity Game Engine (Unity 2019.4.12f)
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Unity_Technologies_logo.svg/1200px-Unity_Technologies_logo.svg.png?width=64" width="70">


# Flow and Diagaram

### Game Flow

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/1fc956d4f350986c200765c4aaabad2c/Flow.png" width ="650">

### Diagram 

* #### Server Architcture 

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/42e0a2709c47b23e2e190ee63cab044e/Server_Architecture.png" width ="650">


 * #### Client Architcture 

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/379d2b39334a0f57b905097b98418f5e/Client_Architecture.png" width ="650">

    
#### Author 
- [Ilham Sugeng Prayogi](https://gitlab.com/IlhamSugengPrayogi17)

- [Raka Arya pratama](https://gitlab.com/notslimboy)


### Release 

You can download the full game [here](https://notslimboy.itch.io/3d-battleground)

### Gameplay 
<img src="Document/Gameplay.gif" width="700">


#### Screenshots
<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/85b7f10465f6c2ab8194543983d764bf/image.png" width ="500">

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/5337584d90ce9ca095797ceb7c488a3a/image.png" width="500">

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/c23a348be7c8d796d2ff165528efe6b6/image.png" width="500">

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/4030197201aa7a96685f87e5f82174c8/image.png" width="500">

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/3149641cc81adbd23f516c4bf065a2e3/image.png" width="500">

<img src="https://gitlab.com/notslimboy/3-d-battleground-multiplayer-online-shooting-game/uploads/2b22211a0cbfb4415ca6cf2f82151ace/image.png" width="500">

#### Game Documentation 

You can see the full game documentaion inclduing game flow and software architecture by click [here](lorem-ipsum)
