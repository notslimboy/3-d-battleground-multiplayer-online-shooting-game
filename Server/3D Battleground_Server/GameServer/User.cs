using System;

[Serializable]
public class User
{
    public string username;
    public string password;

    public string Username
    {
        get { return username; }
        set { username = value; }
    }

    public string Password
    {
        get { return password; }
        set { password = value; }
    }

    public User(string _username, string _password)
    {
        username = _username;
        password = _password;
    }
}