﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

namespace PacketServer
{
    class LobbyManager : MonoBehaviour
    {
        public static LobbyManager instance;

        public int minPlayer = 2;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        public void CheckStartGame()
        {
            int totalPlayer = GetTotalPlayer();
            int isReadyPlayer = GetIsReadyPlayer();
            ServerSend.TotalIsReadyPlayer(totalPlayer,isReadyPlayer);
            if (totalPlayer >= minPlayer && totalPlayer == isReadyPlayer)
            {
                ServerSend.StartGame();
                Debug.Log("Masuk");
                GameManager.instance.StartGame();
            }
        }

        public int GetTotalPlayer()
        {
            int countPlayer = 0;
            foreach (Client _client in Server.clients.Values)
            {
                if (_client.player != null)
                {
                    countPlayer++;
                }
            }
            return countPlayer;
        }

        public int GetIsReadyPlayer()
        {
            int countIsReadyPlayer = 0;
            foreach (Client _client in Server.clients.Values)
            {
                if (_client.player != null && _client.player.isReady)
                {
                    countIsReadyPlayer++;
                    Debug.Log("PlayerCounter: " + countIsReadyPlayer);
                    //ServerSend.TotalIsReadyPlayer(countIsReadyPlayer);
                }
            }
            return countIsReadyPlayer;
            //ServerSend.PlayerIsReady();
        }
        
    }
}
