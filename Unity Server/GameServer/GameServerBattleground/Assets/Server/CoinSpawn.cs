﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PacketServer
{
    public class CoinSpawn : MonoBehaviour
    {
        public static Dictionary<int, Coin> coins = new Dictionary<int, Coin>();
        private int idCount;

        public Vector3 spawnValues;
        
        public float spawnMostWait;
        public float spawnLeastWait;
        private float spawnWait;

        public bool isSpawning;

        [Header("Prefab")]
        public Coin coinPrefab;

        private void Start()
        {
            spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
        }

        private void Update()
        {
            if (!isSpawning) return;
            
            if (spawnWait > 0)
            {
                spawnWait -= Time.deltaTime;
            } 
            else
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), 1, Random.Range(-spawnValues.z, spawnValues.z));
                spawnPosition += this.transform.position;

                ServerSend.SpawnCoin(idCount, spawnPosition);
                Coin coin = Instantiate(coinPrefab, spawnPosition, coinPrefab.transform.rotation) as Coin;
                
                coin.Initialize(idCount);
                idCount++;

                coins.Add(idCount, coin);

                spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
            }

        }
    }
}

