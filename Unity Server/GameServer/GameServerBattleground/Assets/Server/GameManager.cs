﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PacketServer
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        public int minPlayer = 2;
        public CoinSpawn coinSpawner;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        public void StartGame()
        {
            coinSpawner.isSpawning = true;
        }

        public void BackLobby()
        {
            coinSpawner.isSpawning = false;
        }
    }
}

