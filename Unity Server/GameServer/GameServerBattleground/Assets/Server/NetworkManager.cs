using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PacketServer
{
    public class NetworkManager : MonoBehaviour
    {
        public static NetworkManager instance;

        [Header("Default Server Settings")]
        public int port = 26950;
        public int MaxPlayer = 10;
        public GameObject PlayerPrefabs;
        public GameObject projectilePrefab;
        public Transform respawnPosition;
    
        private void Awake()
        {
            if(instance == null)
            {
                instance = this;
            }
            else 
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            //Limit the application to reduce memory usage
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 30;

            Server.Start();
        }

        private void OnApplicationQuit()
        {
            Server.CloseSocket();
        }

        public Player SpawnPlayer()
        {
            Player player = Instantiate(PlayerPrefabs, respawnPosition.position, Quaternion.identity).GetComponent<Player>();
            player.transform.SetParent(this.transform, false);
           
            return player;
        }

        public Projectil InstantiateProjectile(Transform _shootOrigin)
        {
            return Instantiate(projectilePrefab, _shootOrigin.position + _shootOrigin.forward * 0.7f, Quaternion.identity).GetComponent<Projectil>();
        }
    } 
}
