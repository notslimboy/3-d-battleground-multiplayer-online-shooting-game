
using System;
using System.Collections.Generic;
using UnityEngine;


namespace  PacketServer
{
    public class AuthManager : MonoBehaviour
    {
        public static AuthManager instance;

        public List<User> users = new List<User>()
        {
            new User("Raka", "1111"),
            new User("Sugeng", "2222")
        };


        private AuthMessageError authMessageError;
        private enum AuthMessageError
        {
            None,
            WrongPassword,
            UserNotFound,
            UserAlreadyExist
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        public void SignUpInput(int _clientID, string _username, string _password)
        {
            CreateNewUser(_username,_password);
            if (authMessageError != AuthMessageError.None)
            {
                string message = "Sign Up Failed";
                switch (authMessageError)
                {
                    case AuthMessageError.UserAlreadyExist:
                        message = "Username Already Existed";
                        break;
                }

                ServerSend.SignUp(_clientID, false, message);
            }
            else
            {
                string message = "Sign Up Succeed";
                ServerSend.SignUp(_clientID, true, message);
            }
        }


        public void LoginInput(int _clientID, string _username, string _password)
        {
            User _user = CheckLogin(_username, _password);   
            
            if (authMessageError != AuthMessageError.None)
            {
                string message = "Login Failed";
                switch (authMessageError)
                {
                   case AuthMessageError.WrongPassword:
                        message = "Wrong Password";
                        break;
                    case AuthMessageError.UserNotFound:
                        message = "User Not Found !!!";
                        break;
                }
                ServerSend.Login(_clientID, false,message,_user);
            }
            else
            {
                string message = "Login Succeed";
                
                ServerSend.Login(_clientID, true,message,_user);
                Server.clients[_clientID].SendIntoGame(_user);
            }
        }

        public User CheckLogin(string _username, string _password)
        {
            foreach (User _user in users)
            {
                if (_username == _user.username)
                {
                    if (_password != _user.password)
                    {
                        authMessageError = AuthMessageError.WrongPassword;
                        return null;
                    }
                
                    authMessageError = AuthMessageError.None;
                    return _user;
                }
            }

            authMessageError = AuthMessageError.UserNotFound;
            return null;
        }

        public void CreateNewUser(string _username, string _password)
        {
            foreach (User user in users)
            {
                if (_username == user.username)
                {
                    authMessageError = AuthMessageError.UserAlreadyExist;
                    return;
                }
            }

            User _newUser = new User(_username, _password);
            users.Add(_newUser);
            
            authMessageError = AuthMessageError.None;
        }
    }
}


