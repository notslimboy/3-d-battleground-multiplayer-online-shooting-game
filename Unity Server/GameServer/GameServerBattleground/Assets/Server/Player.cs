﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Text;
using TMPro.SpriteAssetUtilities;
using UnityEngine;

namespace PacketServer
{
    public class Player : MonoBehaviour
    {
        public int id;
        public int coin;
        public int zeroCoin = 0;
        public string username;
        public Transform shootOrigin;
        public float health;
        public float maxHealth = 100f;
        public float throwForce = 600f;

        public bool isReady;
        public bool isBack;
        private bool canWalk = true;
        private Animator PlayerAnim;

        private float moveSpeed = 10f / Constants.TICKS_PER_SEC;
        private bool[] inputs;

        public void Initialize(int _id, string _username, Vector3 _spawnPosition)
        {
            id = _id;
            username = _username;
            health = maxHealth;
            //coin = zeroCoin;

            inputs = new bool[4];
        }

        private void Start()
        {
            PlayerAnim = GetComponent<Animator>();
        }

        private void Update()
        {
            if (health <= 0f)
            {
                return;
            }

            Vector2 _inputDirection = Vector2.zero;
            if (inputs[0])
            {
                _inputDirection.y += 1;
            }
            if (inputs[1])
            {
                _inputDirection.x -= 1;
            }
            if (inputs[2])
            {
                _inputDirection.y -= 1;
            }
            if (inputs[3])
            {
                _inputDirection.x += 1;
            }

            Move(_inputDirection);
        }

        private void Move(Vector2 _inputDirection)
        {
            if (canWalk)
            {
                Vector3 _moveDirection = transform.right * _inputDirection.x + transform.forward * _inputDirection.y;
                transform.position += _moveDirection * moveSpeed;
                PlayerAnim.SetBool("isWalk",true);
                ServerSend.SendPlayerAnimation(id, "isWalk",true);
            }
            
            if (_inputDirection == Vector2.zero)
            {
                    
                    PlayerAnim.SetBool("isWalk",false);
                    PlayerAnim.SetBool("isIdle",true);
                    ServerSend.SendPlayerAnimation(id, "isWalk",false);
                    ServerSend.SendPlayerAnimation(id, "isIdle",true);
            }
                

            ServerSend.PlayerPosition(this);
            ServerSend.PlayerRotation(this);
        }

        public void Shoot(Vector3 _viewDirection)
        {
            if (health <= 0)
            {
                return;
            }

            if (Physics.Raycast(shootOrigin.position, _viewDirection, out RaycastHit _hit, 25f))
            {
                PlayerAnim.SetTrigger("isShoot");
                ServerSend.SendPlayerAnimation(id,"isShoot",false);
                if (_hit.collider.CompareTag("Player"))
                {
                    _hit.collider.transform.parent.gameObject.GetComponent<Player>().TakeDamage(50f);
                }
            }    
        }

        public void ThrowItem(Vector3 _viewDirection)
        {
            NetworkManager.instance.InstantiateProjectile(shootOrigin).Initialize(_viewDirection, throwForce, id);
        }

        public void TakeDamage(float _damage)
        {
            if (health <= 0f)
            {
                return;
            }

            health -= _damage;
            if (health <= 0f)
            {
                health = 0f;
                transform.position = NetworkManager.instance.respawnPosition.position;
                ServerSend.PlayerPosition(this);
                StartCoroutine(PlayerRespawn());
            }

            ServerSend.PlayerHealth(this);
        }

        public void SetInput(bool[] _inputs, Quaternion _rotation)
        {
            inputs = _inputs;
            transform.rotation = _rotation;
        }

        public void AddCoins()
        {
            coin++;
            ServerSend.PlayerCoin(id, coin);

            if (coin == 10)
            {
                ServerSend.WinnerPlayer(id);
                coin = zeroCoin;
            }
        }

        private IEnumerator PlayerRespawn()
        {
            yield return new WaitForSeconds(5f);

            health = maxHealth;
            coin = zeroCoin;
            ServerSend.PlayerRespawned(this);
        }
    }
}
