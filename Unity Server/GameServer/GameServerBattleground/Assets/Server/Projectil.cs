﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PacketServer
{
    public class Projectil : MonoBehaviour
    {
        public static Dictionary<int, Projectil> projectiles = new Dictionary<int, Projectil>();
        private static int nextProjectileId = 1;

        public int id;
        public Rigidbody rigidBody;
        public int thrownByPlayer;
        public Vector3 initialForce;
        public float explosionRadius = 1.5f;
        public float explosionDamage = 50f;

        private void Start()
        {
            id = nextProjectileId;
            nextProjectileId++;
            projectiles.Add(id, this);

            ServerSend.SpawnProjectile(this, thrownByPlayer);

            rigidBody.AddForce(initialForce);
            StartCoroutine(ExplodeAfterTime());
        }

        private void FixedUpdate()
        {
            ServerSend.ProjectilePosition(this);
        }

        private void OnCollisionEnter(Collision collision)
        {
            Explode();
        }

        public void Initialize(Vector3 _initialMovementDirection, float _initialForceStrength, int _thrownByPlayer)
        {
            initialForce = _initialMovementDirection * _initialForceStrength;
            thrownByPlayer = _thrownByPlayer;
        }

        private void Explode()
        {
            ServerSend.ProjectileExploded(this);

            Collider[] _colliders = Physics.OverlapSphere(transform.position, explosionRadius);
            foreach (Collider _collider in _colliders)
            {
                if (_collider.CompareTag("Player"))
                {
                    //_collider.GetComponent<Player>().TakeDamage(explosionDamage);
                    _collider.transform.parent.gameObject.GetComponent<Player>().TakeDamage(25f);
                }
            }

            Destroy(gameObject);
        }

        private IEnumerator ExplodeAfterTime()
        {
            yield return new WaitForSeconds(10f);

            Explode();
        }
    }
}
